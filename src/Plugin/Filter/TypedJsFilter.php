<?php

namespace Drupal\typedjs_filter\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\Annotation\Filter;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a base filter for Text Rotator filter.
 *
 * @Filter(
 *   id = "filter_typed_js",
 *   module = "typedjs",
 *   title = @Translation("TypedJS filter"),
 *   description = @Translation("Enables simple typedJS filter using
 *   <code>[typedjs]foo|bar[/typedjs]</code> syntax"), type =
 *   Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE, cache =
 *   FALSE, weight = 0
 * )
 */
class TypedJsFilter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'typeSpeed' => 100,
        'startDelay' => 0,
        'backSpeed' => 10,
        'backDelay' => 1000,
        'smartBackspace' => TRUE,
        'shuffle' => FALSE,
        'fadeOut' => FALSE,
        'fadeOutClass' => 'typed-fade-out',
        'fadeOutDelay' =>  500,
        'loop' => TRUE,
        'loopCount' => -1,
        'showCursor' => TRUE,
        'cursorChar' => '|',
        'attr' => '',
        'autoInsertCss' => TRUE,
        'contentType' => 'html',
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['typeSpeed'] = [
      '#type' => 'number',
      '#title' => $this->t('Typed speed'),
      '#default_value' => $this->settings['typeSpeed'],
      '#description' => $this->t('The average type speed between letters in milliseconds.'),
      '#min' => 0,
    ];
    $form['startDelay'] = [
      '#type' => 'number',
      '#title' => $this->t('Start delay'),
      '#default_value' => $this->settings['startDelay'],
      '#description' => $this->t('The delay before animation starts.'),
      '#min' => 0,
    ];
    $form['backSpeed'] = [
      '#type' => 'number',
      '#title' => $this->t('Back speed'),
      '#default_value' => $this->settings['backSpeed'],
      '#description' => $this->t('Backspacing speed in milliseconds.'),
      '#min' => 0,
    ];
    $form['backDelay'] = [
      '#type' => 'number',
      '#title' => $this->t('Back delay'),
      '#default_value' => $this->settings['backDelay'],
      '#description' => $this->t('The delay before backspace starts.'),
      '#min' => 0,
    ];
    $form['smartBackspace'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Smart Backspace'),
      '#default_value' => $this->settings['smartBackspace'],
      '#description' => $this->t('Only backspace what doesn\'t match the previous string.'),
    ];
    $form['shuffle'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Shuffle'),
      '#default_value' => $this->settings['shuffle'],
      '#description' => $this->t('Shuffles the string.'),
    ];
    $form['fadeOut'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Fade Out'),
      '#default_value' => $this->settings['fadeOut'],
      '#description' => $this->t('Fade out instead of backspace.'),
    ];
    $form['fadeOutClass'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Fade Out Class'),
      '#default_value' => $this->settings['fadeOutClass'],
      '#description' => $this->t('CSS classes for backspace animations.'),
    ];
    $form['fadeOutDelay'] = [
      '#type' => 'number',
      '#title' => $this->t('Fade Out Delay'),
      '#default_value' => $this->settings['fadeOutDelay'],
      '#description' => $this->t('Fade out delay in milliseconds.'),
      '#min' => 0,
    ];
    $form['loop'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Loop'),
      '#default_value' => $this->settings['loop'],
      '#description' => $this->t('Loops the strings.'),
    ];
    $form['loopCount'] = [
      '#type' => 'number',
      '#title' => $this->t('Loop Count'),
      '#default_value' => $this->settings['loopCount'],
      '#description' => $this->t('The amount of loops (-1 for infinity).'),
      '#min' => -1,
    ];
    $form['showCursor'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show Cursor'),
      '#default_value' => $this->settings['showCursor'],
      '#description' => $this->t('Show the cursor.'),
    ];
    $form['cursorChar'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cursor Character'),
      '#default_value' => $this->settings['cursorChar'],
      '#description' => $this->t('Character for the cursor.'),
    ];
    $form['autoInsertCss'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Auto insert CSS'),
      '#default_value' => $this->settings['autoInsertCss'],
      '#description' => $this->t("Insert CSS for cursor and fadeOut into HTML <head>."),
    ];
    $form['attr'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Attribute'),
      '#default_value' => $this->settings['attr'],
      '#description' => $this->t('Attribute for typing (Ex: input placeholder, value, or just HTML text).'),
    ];
    $form['contentType'] = [
      '#type' => 'select',
      '#title' => $this->t('Content Type'),
      '#default_value' => $this->settings['contentType'],
      '#description' => $this->t("'html' or 'null' for plaintext"),
      '#options' => [
        'html' => $this->t('html'),
        'null' => $this->t('null')
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    if (!isset($configuration['settings']['typeSpeed'])) {
      $configuration['settings'] = $this->defaultConfiguration();
    }
    parent::setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $pattern = "/(\[typedjs\])(.*?)(\[\/typedjs\])/";
    $count = 0;
    $strings = [];
    $text = preg_replace_callback($pattern, function ($matches) use (&$count, &$strings) {
      $count++;
      $elemId = Html::getUniqueId('typedjs');
      $strings[$elemId] = explode('|', $matches[2]);
      return '<span id="' . $elemId . '" class="typedjs"></span>';
    }, $text, $limit = -1, $count);
    $result = new FilterProcessResult($text);
    if ($count) {
      $result->addAttachments([
        'library' => [
          'typedjs_filter/typedjs_filter',
        ],
      ]);
      $result->addAttachments([
        'drupalSettings' => [
          'typedjs' => [
            'strings' => $strings,
            'settings' => [
              'typeSpeed' => (int)$this->settings['typeSpeed'],
              'startDelay' => (int)$this->settings['startDelay'],
              'backSpeed' => (int)$this->settings['backSpeed'],
              'backDelay' => (int)$this->settings['backDelay'],
              'smartBackspace' => (bool)$this->settings['smartBackspace'],
              'shuffle' => (bool)$this->settings['shuffle'],
              'fadeOut' => (bool)$this->settings['fadeOut'],
              'fadeOutClass' => (bool)$this->settings['fadeOutClass'],
              'fadeOutDelay' =>  (int)$this->settings['fadeOutDelay'],
              'loop' => (bool)$this->settings['loop'],
              'loopCount' => (int)$this->settings['loopCount'],
              'showCursor' => (bool)$this->settings['showCursor'],
              'cursorChar' => $this->settings['cursorChar'],
              'attr' => $this->settings['attr'],
              'autoInsertCss' => (bool)$this->settings['autoInsertCss'],
              'contentType' => $this->settings['contentType'],
            ],
          ],
        ],
      ]);
    }
    return $result;
  }

}
