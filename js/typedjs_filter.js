/**
 * @file
 * Description.
 */

(function ($, Drupal, drupalSettings) {

  Drupal.behaviors.typedjs = {
    attach: function (context, settings) {
      for (var key in drupalSettings.typedjs.strings) {
        if (drupalSettings.typedjs.strings.hasOwnProperty(key)) {
          const elemId = '#' + key;
          $(elemId, context).once().each(function() {
            const options = drupalSettings.typedjs.settings;
            options['strings'] = drupalSettings.typedjs.strings[key];
            new Typed(elemId, options);
          })
        }
      }
    }
  }
})(jQuery, Drupal, drupalSettings);